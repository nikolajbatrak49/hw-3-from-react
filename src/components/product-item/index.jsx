import { useState, useEffect } from "react";
import PropTypes from "prop-types";

import styles from "../../styles/product.module.scss";
import { Button } from "../button";

import favoriteStar from "../../img/favourites-star.png";
import favoriteStarBlack from "../../img/favourites-star-black.png";

export default function Product({
  openModal,
  updateFavorite,
  updateBasket,
  ...props
}) {
  const [inBasket, setInBasket] = useState(false);
  const [inFavorite, setInFavorite] = useState(false);
  const [product, setProduct] = useState({});
  const [page, setPage] = useState("");

  useEffect(() => {
    setProduct(props.product);

    // перевірка продукту на обране
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const productInFavorite = productsInFavoriteArray.find(
      (product) => product.article === props.product.article
    );
    productInFavorite && setInFavorite(true);

    // перевірка продукту чи в корзині
    const productsInBasketArray = JSON.parse(
      localStorage.getItem("productsInBasket")
    );
    const productInBasket = productsInBasketArray.find(
      (product) => product.article === props.product.article
    );
    productInBasket && setInBasket(true);
  }, [props.product]);

  useEffect(() => {
    setPage(props.page);
  }, [props.page]);

  function addToFavorite() {
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const productInFavorite = productsInFavoriteArray.find(
      (item) => item.article === product.article
    );
    if (productInFavorite) return false;
    localStorage.setItem(
      "productsInFavorite",
      JSON.stringify([...productsInFavoriteArray, product])
    );
    setInFavorite(true);
    updateFavorite();
  }

  function removeFromFavorite() {
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const removedProductIndex = productsInFavoriteArray.findIndex(
      (item) => item.article === product.article
    );
    const updatedproductsInFavoriteArray = [
      ...productsInFavoriteArray.slice(0, removedProductIndex),
      ...productsInFavoriteArray.slice(removedProductIndex + 1),
    ];
    localStorage.setItem(
      "productsInFavorite",
      JSON.stringify(updatedproductsInFavoriteArray)
    );
    setInFavorite(false);
    updateFavorite();
  }

  function toggleFavorite() {
    inFavorite ? removeFromFavorite() : addToFavorite();
  }

  function addToBasket() {
    const productsInBasketArray = JSON.parse(
      localStorage.getItem("productsInBasket")
    );
    const productInBasket = productsInBasketArray.find(
      (item) => item.article === product.article
    );
    if (productInBasket) return false;
    localStorage.setItem(
      "productsInBasket",
      JSON.stringify([...productsInBasketArray, product])
    );
    setInBasket(true);
    updateBasket();
  }

  function removeFromBasket() {
    const productsInBasketArray = JSON.parse(
      localStorage.getItem("productsInBasket")
    );
    const removedProductIndex = productsInBasketArray.findIndex(
      (item) => item.article === product.article
    );
    const updatedProductsInBasketArray = [
      ...productsInBasketArray.slice(0, removedProductIndex),
      ...productsInBasketArray.slice(removedProductIndex + 1),
    ];
    localStorage.setItem(
      "productsInBasket",
      JSON.stringify(updatedProductsInBasketArray)
    );
    setInBasket(false);
    updateBasket();
  }

  const { title, price, imgUrl, company } = product;
  return (
    <div className={styles.Product}>
      <div className={styles.Favorite} onClick={toggleFavorite}>
        <img src={inFavorite ? favoriteStarBlack : favoriteStar} alt="" />
      </div>
      <div className={styles.imageWrapper}>
        <img src={imgUrl} alt="" />
      </div>
      <div>
        <h2>{title}</h2>
        <h4>Марка: {company}</h4>
        <h3>Ціна: {price} грн</h3>
      </div>
      <Button
        text={
          (page === "basket" && "Видалити?") || inBasket
            ? "Додано в кошик"
            : "До кошика"
        }
        backgroundColor={inBasket ? "grey" : "orange"}
        onClick={() => {
          (page === "home" || page === "favorite") && !inBasket
            ? openModal("addToBasket", addToBasket, product.title)
            : page === "basket" &&
              openModal("removeFromBasket", removeFromBasket, product.title);
        }}
      />
    </div>
  );
}

Product.propTypes = {
  product: PropTypes.object,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};
