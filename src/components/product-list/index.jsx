import PropTypes from "prop-types";

import styles from "../../styles/productList.module.scss";
import Product from "../product-item";

export default function ProductList({
  products,
  updateBasket,
  updateFavorite,
  openModal,
  page,
}) {
  return (
    <div className={styles.ProductList}>
      {products.map((product) => (
        <Product
          product={product}
          updateFavorite={updateFavorite}
          updateBasket={updateBasket}
          openModal={openModal}
          key={product.article}
          page={page}
        />
      ))}
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.array,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};
