import PropTypes from "prop-types";
import styles from "../../styles/Button.module.scss";

function Button({ text, backgroundColor, onClick }) {
  return (
    <button
      className={styles.button}
      onClick={onClick}
      style={{ backgroundColor: backgroundColor }}
    >
      {text}
    </button>
  );
}

Button.defaultProps = {
  backgroundColor: "orange",
};

Button.propTypes = {
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
};

export default Button;
