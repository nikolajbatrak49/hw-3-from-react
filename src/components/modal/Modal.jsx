import { useEffect, useState } from "react";
import PropTypes from "prop-types";

import { Button } from "../button";
import modalSettings from "./modalSetting";
import "../../styles/modal.scss";

function Modal({ modalId, closeModal, submitFunction, data, visible }) {
  const [settings, setSettings] = useState({});

  useEffect(() => {
    const modal = modalSettings.find((item) => item.modalId === modalId);
    setSettings(modal.settings);
  }, [modalId, visible]);

  const { header, closeButton, text, actions } = settings;
  return (
    <div className={visible ? "modal active" : "modal"} onClick={closeModal}>
      <div
        className={visible ? "modal-body active" : "modal-body"}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={"modal-header"}>
          {header ? <h2>{header}</h2> : null}
          {closeButton && (
            <Button
              text="X"
              backgroundColor="rgb(163, 35, 0)"
              onClick={closeModal}
            />
          )}
        </div>
        <div className={"modal-content"}>{text && text(data)}</div>
        <div className={"modal-footer"}>
          {actions &&
            actions.map((item, index) => (
              <Button
                text={item.text}
                onClick={
                  item.type === "submit"
                    ? () => {
                        submitFunction();
                        closeModal();
                      }
                    : closeModal
                }
                key={Date.now() + index}
              />
            ))}
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  visible: PropTypes.bool,
  modalId: PropTypes.string,
  submit: PropTypes.func,
  closeModal: PropTypes.func,
};

export default Modal;
