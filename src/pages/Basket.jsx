import ProductList from "../components/product-list";

export function Basket({
  updateBasket,
  updateFavorite,
  openModal,
  products,
  page,
}) {
  return products.length ? (
    <>
      <ProductList
        products={products}
        updateBasket={updateBasket}
        updateFavorite={updateFavorite}
        openModal={openModal}
        page={page}
      />
    </>
  ) : (
    "Корзина пуста"
  );
}
