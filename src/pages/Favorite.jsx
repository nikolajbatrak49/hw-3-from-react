import ProductList from "../components/product-list";

export function Favorite({ products, updateFavorite, updateBasket, openModal, page }) {
  return products.length ? (
    <>
      <ProductList
        products={products}
        updateFavorite={updateFavorite}
        updateBasket={updateBasket}
        openModal={openModal}
        page={page}
      />
    </>
  ) : (
    "Товарів поки немає ):"
  );
}
